import os
import select
import subprocess
import sys
import time

import webbrowser
from termcolor import colored

def print_final():
    result = []
    infile = open('total.txt', 'r')

    for line in infile:
        if line.find("TOTAL") >= 0:
            result = line.split()
            if str(result[3] == "100%"):
                print (str(result[0]) +" ===> "+ colored(str(result[3]), 'green'))
            else:
                print (str(result[0]) +" ===> "+ colored(str(result[3]), 'red'))
        result = []
    infile.close()



def print_number():
    result = []
    infile = open('total.txt', 'r')

    for line in infile:
        if line.find("TOTAL") >= 0 or line.find("total") >= 0:
            result = line.split()
            if str(result[3] == "100%"):
                print (str(result[0]) +" ===> "+ colored(str(result[1]), 'green'))
            else:
                print (str(result[0]) +" ===> "+ colored(str(result[1]), 'red'))
        result = []
    infile.close()


def print_all():
    result = ""
    infile = open('total.txt', 'r')

    for line in infile:
        if line.find("TOTAL") >= 0 or line.find("total") >= 0:
            result = line
    if (result.find("100") >= 0):
        print colored(str(result), 'green')
    else:
        print colored(str(result), 'red')
    infile.close()


def print_cat():
    result = []
    infile = open('total.txt', 'r')

    for line in infile:
        if line.find("mod") >= 0:
            result = line.split()
            if str(result[3] == "100%"):
                print (str(result[0]) +" ===> "+ colored(str(result[3]), 'green'))
            else:
                print (str(result[0]) +" ===> "+ colored(str(result[3]), 'red'))
        result = []
    infile.close()


def redirect(option, pack_name):
    cmd = "/bin/bash"
    if (option == "e"):
        if (pack_name == "build"):
            outfile = open('temp.txt', 'w')
            cmd = "nosetests --with-coverage --cover-html test_built_in.py"
        if (pack_name == "alias"):
            outfile = open('temp.txt', 'w')
            cmd = "nosetests --with-coverage --cover-html test_alias.py"
    else:
        outfile = open('total.txt', 'w')
        cmd = "nosetests --with-coverage --cover-html total.py"

	
    enfant = subprocess.Popen(
            cmd,
            shell = True,
            stdin = subprocess.PIPE,
            stdout = subprocess.PIPE,
            stderr = subprocess.STDOUT
            )

    entree_fermee = False
    sortie_fermee = False
    while True:
        entrees = []
        for f, e in [(entree_fermee, sys.stdin), (sortie_fermee, enfant.stdout)]:
            if not f:
                entrees.append(e)
        pret, _, _ = select.select(entrees, [], [])
        if sys.stdin in pret:
            data = os.read(sys.stdin.fileno(), 1024)
            if len(data) > 1:
                enfant.stdin.write(data)
            else:
                enfant.stdin.close()
                entree_fermee = True
            if enfant.stdout in pret:
                data = os.read(enfant.stdout.fileno(), 1024)
                if len(data) > 1:
                    """sys.stdout.write(data)  Do not display any verbosity"""
                    outfile.write(data)
                    outfile.close()
                    if (option == "f"):
                        print_final()
                    elif (option == "n"):
                        print_number()
                    elif (option == "c"):
                        print_cat()
                    elif (option == "a"):
                        sys.stdout.write(data)
                        print_all()
                    elif (option == "e"):
                        sys.stdout.write(data)

                    print("Generating html files ...")
                    time.sleep(2)
                    os.system("mkdir cover/images")
                    os.system("cp images/bg_lr_1_small.png cover/images")
                    os.system("cp images/bg_lr_2_small.png cover/images")
                    os.system("cp images/bg_lr_3_small.png cover/images")
                    os.system("cp style.css cover/")
                    webbrowser.open('cover/index.html')
                    sys.exit(1)
                else:
                    sortie_fermee = True
                    break

    code_sortie = enfant.wait()
    sys.exit(code_sortie)


