import sys, os
import optparse
import virtualenv

#import all the test over here !
import redirection


def command_e(option, opt_str, value, parser):
    redirection.redirect("e", value)
    return

def command_c(option, opt_str, value, parser):
    redirection.redirect("c", value)
    return

def command_a(option, opt_str, value, parser):
    redirection.redirect("a", value)
    return

def command_f(option, opt_str, value, parser):
    redirection.redirect("f", value)
    return

def command_n(option, opt_str, value, parser):
    redirection.redirect("n", value)
    return
    


def main():
    parser = optparse.OptionParser("usage: %prog [options] arg1")

    parser.add_option("-e", "--select",
            action="callback",
            callback=command_e,
            default="None",
            type="string",
            nargs=1,
            help="execute the test suite on the categories passed in argument only.")

    parser.add_option("-c", "--categories",
            action="callback",
            callback=command_c,
            default="None",
            type="string",
            nargs=0,
            help="display the categories and the percentage of successful tests for each of these categories.")

    parser.add_option("-f", "--final", 
            action="callback",
            callback=command_f,
            default="None",
            type="string",
            nargs=0,
            help="display the percentage of successful tests for all the tests only.")

    parser.add_option("-n", "--number",
            action="callback",
            callback=command_n,
            default="None",
            type="string",
            nargs=0,
            help="display the number of successful tests on the total number of tests")

    parser.add_option("-a", "--all",
            action="callback",
            callback=command_a,
            default="None",
            type="string",
            nargs=0,
            help="execute the test suite on all categories")

    (options, args) = parser.parse_args()
    

if __name__=="__main__":
    main()

