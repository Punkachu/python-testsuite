from distutils.core import setup, Extension
 
module1 = Extension('check_alias', sources = ['aliasmodule1.c'])
module2 = Extension('get_key', sources = ['aliasmodule2.c'])
module3 = Extension('get_cmd', sources = ['aliasmodule3.c'])

setup (name = 'PackageName',
        version = '1.0',
        description = 'check basic handle of alias',
        ext_modules = [module1, module2, module3])

